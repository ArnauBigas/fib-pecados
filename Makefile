AS = sisa-as
LD = sisa-ld
CC = sisa-gcc
OBJCOPY = sisa-objcopy
OBJDIR = build
BINDIR = bin
HEXDIR = hex
PLACADIR = placa

SYS_LDFLAGS= -T system.lds
USR_LDFLAGS= -T user.lds

.PHONY: clean directories

all: directories \
    $(BINDIR)/syscode.bin $(BINDIR)/sysdata.bin \
    $(BINDIR)/idlecode.bin \
    $(BINDIR)/pintaredcode.bin \
    $(BINDIR)/pintabluecode.bin \
    $(BINDIR)/fibonaccicode.bin \
    $(BINDIR)/corre_letrascode.bin $(BINDIR)/corre_letrasdata.bin

all-hex: directories \
    $(HEXDIR)/syscode.hex $(HEXDIR)/sysdata.hex \
    $(HEXDIR)/idlecode.hex \
    $(HEXDIR)/pintaredcode.hex \
    $(HEXDIR)/pintabluecode.hex \
    $(BINDIR)/fibonaccicode.hex \
    $(BINDIR)/corre_letrascode.hex $(BINDIR)/corre_letrasdata.hex

all-placa: directories \
    $(BINDIR)/syscode.bin $(BINDIR)/sysdata.bin \
    $(BINDIR)/idlecode.bin \
    $(BINDIR)/pintaredcode.bin \
    $(BINDIR)/pintabluecode.bin \
    $(BINDIR)/fibonaccicode.bin \
    $(BINDIR)/corre_letrascode.bin $(BINDIR)/corre_letrasdata.bin
	cp  $(BINDIR)/syscode.bin $(PLACADIR)/NOPETES.PLS
	cp  $(BINDIR)/sysdata.bin $(PLACADIR)/ENTRETEN.DAT
	cp  $(BINDIR)/pintaredcode.bin $(PLACADIR)/JUANJO.SO2
	echo "BAKABAKA" >  $(PLACADIR)/JUANJO.SOA
	cp  $(BINDIR)/fibonaccicode.bin $(PLACADIR)/YOLANDA.SO2
	echo "BAKABAKA" >  $(PLACADIR)/YOLANDA.SOA

directories:
	mkdir -p $(BINDIR)
	mkdir -p $(HEXDIR)
	mkdir -p $(OBJDIR)
	mkdir -p $(PLACADIR)
	mkdir -p $(OBJDIR)/sys

$(HEXDIR)/%.hex: $(BINDIR)/%.bin
	od -An -x -v $^ | tr " " "\n" | sed -r '/^\s*$$/d' > $@

include sys/Makefile
include usr/idle/Makefile
include usr/pinta_red/Makefile
include usr/pinta_blue/Makefile
include usr/fibonacci/Makefile
include usr/corre_letras/Makefile

$(OBJDIR)/%.bin: $(OBJDIR)/%.elf
	$(OBJCOPY) -O binary $^ $@

clean:
	rm -rf build bin

emul:
	sisa-emu -t -l addr=0x4000,file=bin/idlecode.bin bin/syscode.bin bin/sysdata.bin
