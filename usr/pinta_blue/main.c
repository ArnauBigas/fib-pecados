unsigned int *screen = (unsigned int*) 0xA000;

void clear_screen(void)
{
    int aux1, aux2, aux3;

    __asm__ (  
            "movi  %0, lo(0xA000)\n\t"   //0xA000 direccion de inicio de la memoria de video
            "movhi %0, hi(0xA000)\n\t"
            "movi  %1, lo(2400)\n\t"     //(80*30=2400=0x0960) numero caracteres de la pantalla
            "movhi %1, hi(2400)\n\t"
            "movi  %2, lo(0x0342)\n\t"
            "movhi %2, hi(0x0342)\n\t"   //un espacio en color negro
            "__repe: st 0(%0), %2\n\t"
            "addi  %0, %0,2\n\t"
            "addi  %1, %1,-1\n\t"
            "bnz   %1, __repe\n\t"
            : /* sin salidas*/
            : "r" (aux1),
              "r" (aux2),
              "r" (aux3));  
}

__attribute__((section(".text.main")))
int main() {
    while(1) clear_screen();
}
