.globl getTicks
getTicks:
    addi r7, r7, 2
    st 0(r7), r0
    movi r0, 0
    calls r0 ;r1 contains return value
    ld r0, 0(r7)
    addi r7, r7, -2
    jmp r5
