# PECADOS

Projecte d'Enginyeria de Computadors Axxx Dxxx Operating System.

## Directory Structure
```
.                    <- Root
|-- Makefile         <- Main Makefile (contains things like C/L flags)
|-- README.md        <- This file
|-- bin              <- Folder containing the binary output
|   |-- syscode.bin  <- System code
|   `-- sysdata.bin  <- System data
|-- build            <- Temporary folder for intermidiate build artifacts
`-- sys              <- Kernel folder
    |-- Makefile     <- Kernel Makefile (contains kernel-specific flags etc)
    `-- src          <- Kernel source files
```

## System Register Usage

- S0: User Status Word
- S1: Return Address
- S2: Event Type
- S3: Syscall number
- S4: Unused
- S5: RSG Address
- S6: System Stack Address
- S7: Status Word

## Virtual Memory Map

```
+----+ <- 0xFFFF
|    |
|    | System Code (Protected, Read Only)
|    |
+----+ <- 0xC000
|    |
|    | Video RAM
|    |
+----+ <- 0xA000
|    |
|    | System Data (Protected)
|    |
+----+ <- 0x8000
|    |
|    | ???
|    |
+----+ <- 0x6000
|    |
|    | User Code?
|    |
+----+ <- 0x4000
|    |
|    | User Data?
|    |
+----+ <- 0x2000
|    |
|    | ???
|    |
+----+ <- 0x0000

```

## Initial TLB contents of the processor

- 0x0
- 0x1
- 0x2
- 0x8 (Protected)
- 0xC (Protected, Read Only)
- 0xD (Protected, Read Only)
- 0xE (Protected, Read Only)
- 0xF (Protected, Read Only)

Nota: Potser hauriem de canviar aquesta distribució inicial perquè no ens és gaire útil, ja que carregarem el codi d'usuari de la sd.
