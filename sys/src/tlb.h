#ifndef TLB_H
#define TLB_H

#define PAGE_TABLE_SIZE 8 //User-space only has access to the lower 32KB of mem

typedef unsigned char PageTag;
typedef PageTag PageTable[PAGE_TABLE_SIZE];

//TLB Exception Handlers
void itlbMiss(PageTag tag);
void dtlbMiss(PageTag tag);
void itlbInvalid(PageTag tag);
void dtlbInvalid(PageTag tag);

void setITLBPage(unsigned char position, PageTag vtag, PageTag ptag,
                 unsigned char valid, unsigned char readOnly);

void setDTLBPage(unsigned char position, PageTag vtag, PageTag ptag,
                 unsigned char valid, unsigned char readOnly);

void softwareTLBFlush();

#endif /* TLB_H */
