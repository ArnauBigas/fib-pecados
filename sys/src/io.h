#ifndef IO_H
#define IO_H

extern unsigned char buttons;
extern unsigned char switches;

int readKeyboard(unsigned char *buf, int size);

void stop7seg();
void set7seg(int num);
void setRLED(int val);
void setGLED(int val);

#endif /* IO_H */
