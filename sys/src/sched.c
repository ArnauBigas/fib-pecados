#include "sched.h"

#include "task.h"
#include "tlb.h"

void schedNext() {
    softwareTLBFlush();
    Task *t = currentTask();
    switchTask(t->next);
}
