#ifndef MEMADDR_H
#define MEMADDR_H

#define USR_STACK_ADDR 0x4000
#define USR_CODE_ADDR  0x4000

#define PAGE_BITS 12
#define PAGE_SIZE 1 << PAGE_BITS

#define USR_STACK_PAGE (USR_STACK_ADDR >> PAGE_BITS) - 1 //STACK_ADDR is at the top
#define USR_CODE_PAGE  USR_CODE_ADDR  >> PAGE_BITS

#endif /* MEMADDR_H */
