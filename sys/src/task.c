#include "task.h"
#include "memaddr.h"

void sysexit(); //Function declared in bootstrap.s

#define INITIAL_QUANTUM 1
#define INITIAL_STATUS 2 // Interrupts enabled, user mode
#define INITIAL_KSP(tasku) (int) &tasku.stack[TASK_RET_OFFSET]

#define TASK_RET_ADDR (int) &sysexit //Function that will be first executed after Task Switch

__attribute__((__section__(".data.task"))) TaskUnion tasks[TASK_NUM];

void initTasks() {
    tasks[0].task.pid = 1;
    tasks[0].task.quantum = INITIAL_QUANTUM;
    tasks[0].task.next = (Task*) 0x8200; //hardcoded perque el compilador no sap fer sumes
    tasks[0].stack[TASK_STATUS_OFFSET] = INITIAL_STATUS;
    tasks[0].stack[TASK_PC_OFFSET] = USR_CODE_ADDR;
    tasks[0].stack[TASK_SP_OFFSET] = USR_STACK_ADDR;
    tasks[0].stack[TASK_RET_OFFSET] = TASK_RET_ADDR;
    tasks[0].task.instPageTable[USR_CODE_PAGE] =  0;
    tasks[0].task.dataPageTable[USR_STACK_PAGE] = 1;
    tasks[0].task.kernelSP = 0x81EA; //Hardcoded perque el compilador no sap fer sumes

    tasks[1].task.pid = 2;
    tasks[1].task.quantum = INITIAL_QUANTUM;
    tasks[1].task.next = (Task*) 0x8000; //hardcoded perque el compilador no sap fer sumes
    tasks[1].stack[TASK_STATUS_OFFSET] = INITIAL_STATUS;
    tasks[1].stack[TASK_PC_OFFSET] = USR_CODE_ADDR;
    tasks[1].stack[TASK_SP_OFFSET] = USR_STACK_ADDR;
    tasks[1].stack[TASK_RET_OFFSET] = TASK_RET_ADDR;
    tasks[1].task.instPageTable[USR_CODE_PAGE] =  2;
    tasks[1].task.dataPageTable[USR_STACK_PAGE] = 3;
    tasks[1].task.kernelSP = 0x83EA; //hardcoded perque el compilador no sap fer sumes
}

Task *currentTask() {
    unsigned int tmp;
    asm("rds %0, s6" : "=r" (tmp));
    return (Task*) (tmp - ((unsigned int)sizeof(TaskUnion)));
}
