#include "cbuffer.h"

unsigned char buttons;
unsigned char switches;

#define KBD_BUF_SIZE 16

unsigned char _kbdBuffer[KBD_BUF_SIZE];
CBuffer keyboardBuffer = {.data = _kbdBuffer, .capacity = KBD_BUF_SIZE};

void intButtons() {
    asm("in %0, 7" : "=r" (buttons));
}

void intSwitches() {
    asm("in %0, 8" : "=r" (switches));
}

void intKeyboard() {
    unsigned char code;
    asm("in %0, 15" : "=r" (code));
    cbufferPush(&keyboardBuffer, code);
}

int readKeyboard(unsigned char *buf, int size) {
    int originalSize = size;
    while(size > 0 && keyboardBuffer.size > 0) {
        *(buf++) = cbufferPop(&keyboardBuffer);
        size--;
    }
    return originalSize - size;
}

void stop7seg() {
    const int control = 0;
    asm("out 9, %0" :: "r" (control));
}

void set7seg(int num) {
    const int control = 0xF;
    asm("out 9, %0\n\tout 10, %1" :: "r" (control), "r" (num));
}

void setRLED(int val) {
    asm("out 6, %0" :: "r" (val));
}

void setGLED(int val) {
    asm("out 7, %0" :: "r" (val));
}
