#ifndef TASK_H
#define TASK_H

#include "tlb.h"

#define TASK_STACK_SIZE 256
#define TASK_NUM 2

//Offsets dels differents registres dins l'stack del task
#define TASK_STACK_START   TASK_STACK_SIZE - 1
#define TASK_STATUS_OFFSET TASK_STACK_START - 7
#define TASK_PC_OFFSET     TASK_STACK_START - 8
#define TASK_SP_OFFSET     TASK_STACK_START - 9
#define TASK_RET_OFFSET    TASK_STACK_START - 10

typedef struct task {
    int kernelSP; // IMPORTANT! Sempre ha de ser el primer, s'hi fa referencia en asm
    int pid;
    int quantum;
    int marc_pila; //?
    struct task *next, *prev;
    PageTable instPageTable;
    PageTable dataPageTable;
} Task;

typedef union {
    Task task;
    unsigned int stack[TASK_STACK_SIZE];
} TaskUnion;

extern TaskUnion tasks[TASK_NUM];

void initTasks();

void switchTask(Task *other);

Task *currentTask();

//Explanation for the above: s6 contains at all times the system stack, which
//is related to the task currently being executed (because the task context is
//actually stored in said system stack). Therefore, by checking s6 we can get
//a pointer to the task being executed. Except, this pointer is actually to the
//end of the currently executed task, which is actually the start of the next
//task stored in the array. To get around this we simply subtract the size
//of the Task Union so we get to the start of the task (and therefore the
//actual Task object).

#endif /* TASK_H */
