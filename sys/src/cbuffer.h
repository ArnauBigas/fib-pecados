#ifndef CBUFFER_H
#define CBUFFER_H

typedef struct {
    unsigned char *data; //< Raw buffer containing the actual data
    int head;            //< Position of the next free element
    int tail;            //< Position of the next available element
    int size;            //< Positions used
    int capacity;        //< Positions available
} CBuffer;

void cbufferPush(CBuffer *b, unsigned char val);
unsigned char cbufferPop(CBuffer *b);
int cbufferIsFull(CBuffer *b);

#endif /* CBUFFER_H */
