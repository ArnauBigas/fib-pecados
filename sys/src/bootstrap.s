.include "sys/src/macros.s"

.set PILA, 0x8200 ; Use first task's stack as initial stack pointer

.section .text.main
.globl bootstrap
bootstrap:
    ; Setup RSG
    $MOVEI r1, RSG
    wrs    s5, r1

    ; Setup temporary stack for the initialization (uses task0's stack)
    $MOVEI r7, PILA-20 ; Leave space for task0 stack initialization

    ; Jump to C code for the rest of the init
    $MOVEI r6, init
    jal    r5, r6

.globl sysexit
sysexit:
    ; Finally jump to user code
    ; Setup status word, stack address and system address for initial task
    $restore_all
    reti
