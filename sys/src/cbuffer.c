#include "cbuffer.h"

void cbufferPush(CBuffer *b, unsigned char val) {
    b->data[b->head++] = val;
    if(b->head == b->capacity) b->head = 0;
    b->size++;
}

unsigned char cbufferPop(CBuffer *b) {
    unsigned char val = b->data[b->tail++];
    if (b->tail == b->capacity) b->tail = 0;
    b->size--;
    return val;
}

int cbuffer_full(CBuffer *b) {
    return b->size == b->capacity;
}
