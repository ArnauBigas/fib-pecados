.include "sys/src/macros.s"

.globl switchTask
switchTask:
    addi r7, r7, -2
    st 0(r7), r5 ; store @ret of the current task
    $MOVEI r5, currentTask
    jal r5, r5   ; get current task
    st 0(r1), r7 ; store SP to kernelSP in Task structure
    ld r5, 2(r7) ; load @ of the next Task
    ld r7, 0(r5) ; load SP of the next Task to execute (first field in struct)
    ld r5, 0(r7) ; load @ret of the new task
    addi r7, r7, 2
    jmp r5
