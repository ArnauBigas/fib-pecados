#include "tlb.h"
#include "task.h"
#include "io.h"
#define TLB_ENTRY(tag, valid, ro) (valid << 5) | (ro << 4) | (tag & 15)

void setITLBPage(unsigned char position, PageTag vtag, PageTag ptag,
                 unsigned char valid, unsigned char readOnly)
{
    //set7seg(vtag);
    //setRLED(1);
    unsigned char entry = TLB_ENTRY(ptag, valid, readOnly);
    asm("wrvi %0, %1\n\twrpi %0, %2" :: "r" (position), "r" (vtag), "r" (entry));
    //setRLED(0);
    //stop7seg();
}

void setDTLBPage(unsigned char position, PageTag vtag, PageTag ptag,
                 unsigned char valid, unsigned char readOnly)
{
    //set7seg(vtag);
    //setRLED(2);
    unsigned char entry = TLB_ENTRY(ptag, valid, readOnly);
    asm("wrvd %0, %1\n\twrpd %0, %2" :: "r" (position), "r" (vtag), "r" (entry));
    //setRLED(0);
    //stop7seg();
}

void itlbMiss(PageTag vtag) {
    //set7seg(4);
    setITLBPage(vtag & 3, vtag, currentTask()->instPageTable[vtag], 1, 1);
}

void dtlbMiss(PageTag vtag) {
    //set7seg(5);
    Task *t = currentTask();
    setDTLBPage(vtag & 3, vtag, t->dataPageTable[vtag], 1, 0);
    ((TaskUnion*)t)->stack[TASK_PC_OFFSET] -= 2;
}

void itlbInvalid(PageTag vtag) {
    //set7seg(6);
    setITLBPage(vtag & 3, vtag, currentTask()->instPageTable[vtag], 1, 1);
}

void dtlbInvalid(PageTag vtag) {
    //set7seg(7);
    Task *t = currentTask();
    setDTLBPage(vtag & 3, vtag, t->dataPageTable[vtag], 1, 0);
    ((TaskUnion*)t)->stack[TASK_PC_OFFSET] -= 2;
}

void softwareTLBFlush() {
    //set7seg(8);
    int i;
    for (i = 0; i < 4; i++) {
        setITLBPage(i, 0, 0, 0, 0);
        setDTLBPage(i, 0, 0, 0, 0);
    }
}
