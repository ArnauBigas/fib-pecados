#include "sched.h"
#include "io.h"

unsigned int ticks = 0;

void intTimer() {
    ticks++;
    set7seg(ticks);
    schedNext();
}

int syscallGetTicks() {
    return ticks;
}
