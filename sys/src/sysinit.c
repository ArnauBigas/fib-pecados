#include "io.h"
#include "task.h"
#include "tlb.h"

int load_user();  // Loads user code & data

void init(void) {
  // Modify TLB initial entries to ensure they don't get overwritten by the
  // tasks, since user space can modify the first 4 entries of the TLB
  set7seg(0xC0DE);
  setITLBPage(7, 0xC, 0xC, 1, 1);
  setDTLBPage(7, 0x8, 0x8, 1, 0);
  setDTLBPage(6, 0xB, 0xB, 1, 0);
  setDTLBPage(5, 0xA, 0xA, 1, 0);
  setDTLBPage(4, 0x3, 0x3, 1, 0);
  setDTLBPage(3, 0x2, 0x2, 1, 0);
  setDTLBPage(2, 0x1, 0x1, 1, 0);
  setDTLBPage(1, 0x0, 0x0, 1, 0);

  // Initialize tasks
  set7seg(0xBEEF);
  initTasks();

  set7seg(0xABCD);
  int err = load_user();
  if (err) {
    set7seg(err);
    asm("halt");
  } else {
    set7seg(0xCAFE);
  }
}
