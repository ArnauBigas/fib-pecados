.include "sys/src/macros.s"

; seccion de datos
.data
    .balign 2 ; por si acaso, pero no debería ser necesari

interrupts_vector:
    .word intTimer    ; 0 Interval Timer
    .word intButtons  ; 1 Pulsadores (KEY)
    .word intSwitches ; 2 Interruptores (SWITCH)
    .word intKeyboard ; 3 Teclado PS/2

exceptions_vector:
    .word RSE_default_halt   ;  0 Instrucción ilega
    .word RSE_default_halt   ;  1 Acceso a memoria no alineado
    .word RSE_default_resume ;  2 Overflow en coma flotante
    .word RSE_default_resume ;  3 División por cero en coma flotant
    .word RSE_default_halt   ;  4 División por cer
    .word RSE_default_halt   ;  5 No definida
    .word RSE_excepcion_TLB  ;  6 Miss en TLB de instrucciones
    .word RSE_excepcion_TLB  ;  7 Miss en TLB de datos
    .word RSE_excepcion_TLB  ;  8 Página inválida al TLB de instruccies
    .word RSE_excepcion_TLB  ;  9 Página inválida al TLB de dos
    .word RSE_default_halt   ; 10 Página protegida al TLB de instruccione
    .word RSE_default_halt   ; 11 Página protegida al TLB de dato
    .word RSE_default_halt   ; 12 Página de sólo lectu
    .word RSE_default_halt   ; 13 Excepción de protecció

call_sys_vector:
    .word syscallGetTicks ; 0 Interval Timer
    .word RSI_default_resume ; 1 Pulsadores (KEY)
    .word RSI_default_resume ; 2 Interruptores (SWITCH)
    .word RSI_default_resume ; 3 Teclado PS/2

tlb_vector:
    .word itlbMiss     ;  6 Miss en TLB de instrucciones
    .word dtlbMiss     ;  7 Miss en TLB de datos
    .word itlbInvalid  ;  8 Página inválida al TLB de instruccies
    .word dtlbInvalid  ;  9 Página inválida al TLB de dos

.text

.globl RSG
RSG:
    ; intercanvi de piles
    wrs s4, r7 ; guardem pila usuari a s4
    rds r7, s6 ; restaurem pila sistema a r7

    ; Salvar el estado
    $push  r0, r1, r2, r3, r4, r5, r6
    rds    r0, s0 ; status word
    rds    r1, s1 ; @ retorn
    rds    r2, s4 ; user stack
    $push  r0, r1, r2

    movi r0, 1
    out 7, r0

    ; Procesem el signal
    rds    R1, S2 ;consultamos el contenido de S2
    movi   R2, 14
    cmpeq  R3, R1, R2 ;si es igual a 14 es una llamada a sistema
    bnz    R3, __call_sistema ;saltamos a las llamadas a sistema si S2 es igual a 14
    movi   R2, 15
    cmpeq  R3, R1, R2 ;si es igual a 15 es una interrupció
    bnz    R3, __interrupcion ;saltamos a las interrupciones si S2 es igual a 15
    ; Cuidao! Fall-through!

__excepcion:
    movi   R2, lo(exceptions_vector)
    movhi  R2, hi(exceptions_vector)
    add    R1, R1, R1 ;R1 contiene el identificador de excepció
    add    R2, R2, R1
    ld     R2, 0(R2)
    jal    R6, R2
    bz     R3, __finRSG

__call_sistema:
    rds    R1, S3 ;S3 contiene el identificador de la llamada a sistema
    movi   R2, 7
    and    R1, R1, R2  ;nos quedamos con los 3 bits de menor peso limitar el número de servicios definidos en el S.O
    add    R1, R1, R1
    movi   R2, lo(call_sys_vector)
    movhi  R2, hi(call_sys_vector)
    add    R2, R2, R1
    ld     R2, 0(R2)
    jal    R6, R2
    st 16(r7), r1 ; store result in stack for later restoration
    bnz    R3, __finRSG

__interrupcion:
    getiid R1
    add    R1, R1, R1
    movi   R2, lo(interrupts_vector)
    movhi  R2, hi(interrupts_vector)
    add    R2, R2, R1
    ld     R2, 0(R2)
    jal    R5, R2

__finRSG: ;Restaurar el estado
    movi r1, 0
    out 7, r1
    $restore_all
    reti

RSE_default_halt:
    halt

RSE_default_resume:
    jmp r6

RSI_default_resume:
    jmp r6

RSE_excepcion_TLB:
    ; Pass exception tag as parameter
    rds    r2, s3
    movi   r5, -12
    shl    r2, r2, r5
    addi   r7, r7, -2
    stb    0(r7), r2
    addi   r1, r1, -12 ; r1 contains exception number * 2
    movi   r2, lo(tlb_vector)
    movhi  r2, hi(tlb_vector)
    add    r2, r2, r1
    ld     r2, 0(r2)
    jal    r5, r2
    addi   r7, r7, 2
    jmp    r6
