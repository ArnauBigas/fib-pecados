.include "/sys/src/macros.s"

.set MBR, 0x3000
.set VBR, 0x3000
.set DIR, 0x3200
.set user_code1_name, 0x3400
.set user_data1_name, 0x340C
.set user_code2_name, 0x3418
.set user_data2_name, 0x3424
.set user_code1, 0x0000
.set user_data1, 0x1000
.set user_code2, 0x2000
.set user_data2, 0x3000

; sección de código
.globl load_user
load_user:
    $push r0, r2, r3, r4, r5, r6 ; fem push de tots els registres que farem servir
    $movei r1, 0x3FFC
    st 0(r1), r7 ; fem backup de l'stack pointer a l'ultim block que escriurem
 ; *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
 ; Inicialización
 ; *=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*
; load user1 code file name
    $movei r1, user_code1_name
    $movei r2, 0x554A
    st 0(r1), r2
    $movei r2, 0x4E41
    st 2(r1), r2
    $movei r2, 0x4F4A
    st 4(r1), r2
    $movei r2, 0x2020
    st 6(r1), r2
    $movei r2, 0x4F53
    st 8(r1), r2
    movi r2, 0x32
    stb 10(r1), r2

; load user1 data file name
    $movei r1, user_data1_name
    $movei r2, 0x554A
    st 0(r1), r2
    $movei r2, 0x4E41
    st 2(r1), r2
    $movei r2, 0x4F4A
    st 4(r1), r2
    $movei r2, 0x2020
    st 6(r1), r2
    $movei r2, 0x4F53
    st 8(r1), r2
    movi r2, 0x41
    stb 10(r1), r2

; load user2 code file name
    $movei r1, user_code2_name
    $movei r2, 0x4F59
    st 0(r1), r2
    $movei r2, 0x414C
    st 2(r1), r2
    $movei r2, 0x444E
    st 4(r1), r2
    $movei r2, 0x2041
    st 6(r1), r2
    $movei r2, 0x4f53
    st 8(r1), r2
    movi r2, 0x32
    stb 10(r1), r2

; load user2 data file name
    $movei r1, user_data2_name
    $movei r2, 0x4F59
    st 0(r1), r2
    $movei r2, 0x414C
    st 2(r1), r2
    $movei r2, 0x444E
    st 4(r1), r2
    $movei r2, 0x2041
    st 6(r1), r2
    $movei r2, 0x4f53
    st 8(r1), r2
    movi r2, 0x41
    stb 10(r1), r2

    $MOVEI r1, MBR
    movi r2, 0
    movi r3, 0
    $CALL r6, __sd_read_block ; carreguem el MBR 

    ; Load first partition
    $MOVEI r1, 0x31C6 ; 0x2000 + 0x01BE + 0x8 (LBA partition 1)
    ld r2, 2(r1)
    ld r3, 0(r1)
    bnz r3, __lba_ok
    $movei r1, user_data2
    ld r7, 0(r1)
    $MOVEI r1, 0x0ACA
    $pop r6, r5, r4, r3, r2, r0
    jmp r5
    HALT
__lba_ok:
    and r7, r3, r3 ; Fem backup de R3 a R7
    $MOVEI r1, VBR
    $CALL r6, __sd_read_block ; carreguem el VBR
    $MOVEI r1, VBR
    ld r1, 0(r1)
    bnz r1, __vbr_read_ok
    $movei r1, 0x3FFC
    ld r7, 0(r1)
    $MOVEI r1, 0x0AC2
    $pop r6, r5, r4, r3, r2, r0
    jmp r5
    HALT


__vbr_read_ok:
;;;;;;;;;;;;;;;;;;;;
;; LECTURA DE FAT ;;
;;;;;;;;;;;;;;;;;;;;
    $movei r1, VBR ; r1 <- starting address of the VBR
    ; calculation of reserved sectors to know the start of the FAT area
    ld r5, 14(r1) ; r5 <- # reserved sectors
    ; calculation of the number of sectors of the FAT area to know the
    ; starting sector of the data area
    ldb r6, 16(r1); # of FATs
    ld r0, 22(r1) ; # of sectors per FAT
    mul r6, r6, r0 ; r6 <- # of sectors FAT area
    movi r2, 0
    add r3, r5, r6 ; r3 <- number of sectors from the beggining of the FAT
                   ; partition to the beggining of the data area
    ; the first sector of the data area is loaded, since this sector contains
    ; the root directory in which the file should be located
    add r7, r7, r3 ; r7 contains the starting sector for the FAT partition
    addi r3, r7, 0 ; r3 is one of the parameters for the sd reading function
    $movei r1, DIR ; this block will be loaded in DIR memory address
    $call r6, __sd_read_block 
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ; Calculate start of data blocks address ;
    ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    $movei r1, VBR
    ; HEM DE FER EL LOAD WORD AIXI PQ NO ESTA ALINEAT. VISCA FAT16
    ldb r0, 0x11(r1); low root directory entries
    movhi r0, 0
    ldb r2, 0x12(r1); high root directory entries
    movi r1, 8
    shl r2, r2, r1
    or r2, r0, r2 ; root directory entries
    movi r0, 5
    shl r2, r2, r0
    movi r0, -9
    shl r2, r2, r0
    add r7, r7, r2
    ;;;;;;;;;;;;;;;;
	$movei r1, 0x3FFE
    movi r0, 0
	st 0(r1), r0
__load_file:
	$movei r1, DIR
    $movei r0, 0x3FFE
	ld r2, 0(r0)
    movi r0, 0 ; r0 will be the block counter
	bz r2, __load_user_code1
    movi r3, 1
    cmpeq r3, r2, r3
    bnz r3, __load_user_data1
    movi r3, 2
    cmpeq r3, r2, r3
    bnz r3, __load_user_code2
    $movei r2, user_data2_name
    bnz r2, __lookup_loop
__load_user_code2:
    $movei r2, user_code2_name
    bnz r2, __lookup_loop
__load_user_data1:
    $movei r2, user_data1_name
    bnz r2, __lookup_loop
__load_user_code1:
    $movei r2, user_code1_name
__lookup_loop:
    ldb r4, 0(r2) ; load the byte of the name we want
    ldb r3, 0(r1) ; load the byte of the entry we are looking
    sub r3, r3, r4
    bnz r3, __next_entry ; if they are different check the next entry
    addi r0, r0, 1
    movi r3, 11
    sub r3, r3, r0
    bnz r3, __lookup_loop ; if the counter isn't 11, we haven't found the file yet
    addi r1, r1, 1
    $movei r3, 0x3400
    $cmpge r3, r1, r3
    bnz r3, __file_not_found
    $MOVEI r3, __file_found
    jmp r3  ; otherwise the file was found, time to load it
__next_entry:
    movi r3, 0xE0 ; Posa 0xFFE0 a r3
    and r1, r1, r3 ; mask to start
    movi r3, 32
    add r1, r1, r3
    $movei r3, 0x3400
    $cmpge r3, r1, r3
    bnz r3, __file_not_found ; if we reach the end of the block the file is not there
    bz r3, __lookup_loop
__file_found:
    movi r2, 0xE0 ; Posa 0xFFE0 a r3
    and r1, r1, r2 ; mask to start
    ; first we get the starting cluster of the file
    ld r2, 26(r1)
    ; then we get the file size
    ld r0, 28(r1)
    $movei r5, -9
    shl r1, r0, r5 ; r0 <- file size in  full blocks
    $movei r5, 9
    shl r5, r1, r5
    cmpeq r5, r5, r0 ; mirem si la divisio es exacta
    bz r5, __suma_1_size
    and r0, r1, r1
    bnz r5, __fi_calc_size
__suma_1_size:
    addi r0, r1, 1 ; add remaining block
__fi_calc_size:
    $movei r1, VBR
    addi r2, r2, -2 ; Restem 2 al cluster per a poder calcular la @
    ldb r4, 13(r1) ;r4 <- sectors per cluster
    mul r3, r2, r4 ; r3 <- first sector of the file
    add r3, r7, r3 ; r7 <- starting sd sector of the file
	$movei r1, 0x3FFE
	ld r1, 0(r1)
	bz r1, __load_uc1
    movi r2, 1
    cmpeq r2, r1, r2
    bnz r2, __load_ud1
    movi r2, 2
    cmpeq r2, r1, r2
    bnz r2, __load_uc2
    $movei r1, 0x3FFC
    ld r7, 0(r1) ; en aquest punt ja no necessitarem r7 més així que restaurem l'stack pointer
    $movei r1, user_data2
    bnz r1, __endif
__load_uc2:
    $movei r1, user_code2
    bnz r1, __endif
__load_ud1:
    $movei r1, user_data1
    bnz r1, __endif
__load_uc1:
    $movei r1, user_code1
    bz r1, __endif
__endif:
    movi r2, 0 ; r2 is a parameter for the sd function
__loop_load_kern:
    $call r6,__sd_read_block ; we write a block of the file
    ; La rutina ja ens deixa R1 incrementat a on cal
    addi r3, r3, 1 ; calculate the address for the next sd block
    addi r0, r0, -1
    bnz r0, __loop_load_kern ; if we loaded the whole file jump to code, otherwise loop
    $movei r2, 0x3FFE
	ld r0, 0(r2)
    movi r1, 3
    cmpeq r1, r0, r1
	bnz r1, __exit_user_load
	addi r0, r0, 1
	st 0(r2), r0
	bz r1, __load_file
__file_not_found:
    $movei r1, 0x3FFC
    ld r7, 0(r1)
    $MOVEI r1, 0xFD1E
    $pop r6, r5, r4, r3, r2, r0
    jmp r5


;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SALIDA DEL BOOTLOADER ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;
__exit_user_load:
    movi r1, 0
    $pop r6, r5, r4, r3, r2, r0
    jmp r5
    HALT ;; No hauria d'executar-se mai, pero bueno, ahí está

__sd_read_block:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; guardar els registres a l'stack ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    movi r4, 0
    out 30, r4 ; Fem reset a la SD escrivint qualsevol cosa al port 30
__loop_reset_sd_l:
    in r4, 30
    bnz r4, __loop_reset_sd_l
    ; reset completat
    in r4, 36
    bz r4, __reset_ok_l
    movi r1, -1
    bnz r4, __final_lectura
__reset_ok_l:
    out 32, r3 ; @ inicial de la SD: r2 & r3
    out 33, r2
    movi r4, 1
    out 37, r4 ; Demanem inici de lectura
__loop_inici_l:
    in r4, 30
    bz r4, __loop_inici_l
    ; ja estem busy, podem rebre bytes
    $MOVEI r2, 512
__loop_nou_byte_l:
    in r4, 31
    bz r4, __loop_nou_byte_l
    ; Byte disponible, escrivim a memòria
    in r4, 35 ; Lectura del byte
    out 31, r4 ; avisem de que ja hem fet la lectura
    stb 0(r1), r4 ; Escriptura a memòria
    addi r1, r1, 1; incrementem la @ de memòria
    addi r2, r2, -1 ; i--
    bz r2, __fi_sector_l ; Si hem llegit 512 bytes, ja estem
    bnz r1, __loop_nou_byte_l  ; tornem a l'inici del loop (r1 Mai és 0)  
__fi_sector_l:
    movi r4, 0
    out 37, r4 ; Ja s'ha acabat la lectura, desactivem el bit de lectura.
    in r1, 36 ; Guardem l'error a r1
__final_lectura:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; treure registres de l'stack ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    jmp r6 ; retornem de la crida
